import { Component, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Http } from '@angular/http';


@Component({
  selector: 'app',
  //styleUrls: [],
  encapsulation: ViewEncapsulation.None,
  templateUrl: `app.component.html`
})

export class AppComponent {

  public formContact: FormGroup;
  public enableAddress: boolean = true;
  public showCepError: boolean = false;
  public showEmailError: boolean = false;
  public userList: Array<object> = [];

  constructor(private fb: FormBuilder, private http: Http) {

    // form validation
    this.formContact = fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      tel: this.fb.array([
        this.fb.group({
          number: ['', Validators.required]
        })
      ]),
      obs: [''],
      address: this.fb.array([
        this.fb.group({
          cep: ['', Validators.required],
          street: ['', Validators.required],
          bairro: ['', Validators.required],
          state: ['', Validators.required],
          city: ['', Validators.required],
          addressNumber: ['', Validators.required]
        })
      ])
    })
  }

  addTel(){
    const controlTel = <FormArray>this.formContact.controls['tel'];
    controlTel.push(
      this.fb.group({
          number: ['', Validators.required]
      })
    )
  }

  addAddress(){
    this.showCepError = false;
    const controlAddress = <FormArray>this.formContact.controls['address'];
    controlAddress.push(
      this.fb.group({
          cep: ['', Validators.required],
          street: ['', Validators.required],
          bairro: ['', Validators.required],
          state: ['', Validators.required],
          city: ['', Validators.required],
          addressNumber: ['', Validators.required]
      })
      
    )
  }

  // on submit
  onSubmit() {

    this.showEmailError = false;

    for (let x of this.userList) {
      if (x.email == this.formContact.value['email']) {
        this.showEmailError = true;
        break;
      }
    }

    if( !this.showEmailError ){
      this.userList.push(
        this.formContact.value
      );
      this.showEmailError = false;
    }
  }

  // get address
  getAddress(index) {

    let currentAddress = (this.formContact.controls['address']['controls'][index]);
    
    let cepValue = currentAddress.value['cep'];

    this.http
      .get('http://api.postmon.com.br/v1/cep/' + cepValue)
      .subscribe(res => {
        let address = res.json();

        this.setAddress(index, address);
        this.enableAddress = false;
        this.showCepError = false;

      }, error => {
        this.enableAddress = true;
        this.showCepError = true;

      })
  }

  setAddress(index, address) {
    this.formContact.controls['address']['controls'][index].controls['street'].setValue(address.logradouro);
    this.formContact.controls['address']['controls'][index].controls['bairro'].setValue(address.bairro);
    this.formContact.controls['address']['controls'][index].controls['state'].setValue(address.estado);
    this.formContact.controls['address']['controls'][index].controls['city'].setValue(address.cidade);
  }
}